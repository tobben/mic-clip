module inner_round_corner_2d(r, ang=90, back=0.1, center=false){
  cx = r*(1-cos(ang/2+45));
  if(center){
    translate([-r , -r])
      difference(){
        translate([-back, -back])
          square([cx+back, cx+back]);
        translate([r,r])
          circle(r=r);
      }
  } else {
    translate([-r*(1-sin(ang/2+45)), -r*(1-sin(ang/2+45))])
      difference(){
        translate([-back, -back])
          square([cx+back, cx+back]);
        translate([r,r])
          circle(r=r);
      }
  }
}
module inner_round_corner(r, h, ang=90, back = 0.1, center=false){
  linear_extrude(height=h, convexity=4)
    inner_round_corner_2d(r, ang, back, center);
}

module main(){
  hull(){
    rotate([0,-60,0])
      translate([-34,0,16])
        rotate([0,70,0])
          translate([-2,0,-12+22-12])
            cylinder(d1=28.7+3, d2 = 30.8+3, h=36+12,$fn=39);
    rotate([90,0,0])
      cylinder(d=20, h=7.4, center=true, $fn=40);
  }
}


difference(){
  union(){
    rotate([0,15.5,0]) {
      difference(){
        main();
        rotate([0,-60,0])
          translate([-34,0,16])
            rotate([0,70,0])
              translate([-2,0,-23])
                cylinder(d1=27, d2=31, h=85,$fn=60);
        rotate([0,-60,0])
          translate([-34,0,16])
            rotate([0,70,0])
              translate([-15,0,23])
                cube([10,14,110],center=true);
        translate([1,0,-0.4])
          rotate([90,0,0])
            cylinder(d=8.7, h=20, center=true, $fn=40);
        for(k=[0,1]){
          mirror([0,k,0]){
            rotate([90,0,0])
              translate([0,0,7.4/2])
                cylinder(d=21, h=20, $fn=40);
            rotate([0,11.8,0])
              translate([-10.7-2,7.4/2,-18])
                cube(50);
          }
        }
      }
    }
    intersection(){
      for(k=[0,1])
        mirror([0,k,0])
          rotate([0,15.5,0])
            rotate([0,11.8,0])
              translate([-12.7,7.4/2,-18])
                inner_round_corner(r=2, h=38, $fn=15*4);
      rotate([0,15.5,0])
        main();
    }
  }
  translate([-30,0,-25-10])
    cube([150,50,50], center=true);
  for(k=[0,1]){
    mirror([0,k,0]){
      translate([-35.5,8.0,37.6])
        rotate([0,21,-21])
          rotate([0,90,0])
            rotate([0,0,-5])
              inner_round_corner(r=7, h=10, back=1,$fn=15*4);
      translate([-42,7,-11.4])
          rotate([0,-90,0])
            rotate([0,15,0])
              inner_round_corner(r=7, h=10, back=1,$fn=15*4);
      rotate([0,15.5,0])
        rotate([0,11.8,0])
          translate([-12.34,11.79,-18])
            rotate([0.6,0.25,2*90-10])
              translate([0,0,-1])
                inner_round_corner(r=2, h=39, ang=70, $fn=15*4);
    }
  }
}
